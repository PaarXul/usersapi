# Utiliza una imagen base de Java 17
FROM openjdk:17-jdk-alpine
COPY target/userapi.jar userapi.jar
ENTRYPOINT ["java","-jar","/userapi.jar"]